<?php
global $dbconnect, $current_user, $decrypt_folder, $list_jabatan;


$host = 'localhost'; //Host yang digunakan Localhost atau 127.0.0.1
$user = 'root'; //Username dari Host yakni root
$pass = ''; //User root tidak menggunakan password
$dbname = 'db_aes'; //Nama Database Aplikasi Enkirpsi dan Dekripsi
$dbconnect = mysqli_connect($host, $user, $pass) or die(mysqli_error()); //Mencoba terhubung apabila Host, User, dan Pass Benar. Kalau tidak MySQL memberikan Notif Error
$dbselect = mysqli_select_db($dbconnect, $dbname); //Jika benar maka akan memilih Database sesuai pada variable $dbname

$decrypt_folder = '7e7dfefdac50b9f1e367b9f286d4b2e8';

$list_jabatan = [
    'Pembina Utama Madya - IV/d',
    'Pembina Tk. 1 - IV/b',
    'Pembina - IV/a',
    'Penata Tk. 1 - III/d'
];

include_once ('functions.php');

$current_user = get_current_user_login();
?>
