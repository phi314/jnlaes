<!DOCTYPE html>
<html>
  <head>
    <title>Aplikasi Enkripsi dan Dekripsi Puslitbang Geologi Kelautan</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/images/favicon.png">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="assets/css/main-child.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
    script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
    -->
  </head>
  <body>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo text-center">
          <img src="assets/images/logo - 1.png" alt="" style="width: 350px">
      </div>

        <?php if( isset($_GET['error'])): ?>
        <div class="alert alert-danger">
            <strong>Username dan password anda tidak cocok</strong>
        </div>
        <?php endif; ?>

      <div class="login-box">
        <form class="login-form" action="auth.php" method="post" validate=true>
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>Login</h3>
          <div class="form-group">
            <label class="control-label">Username</label>
            <input class="form-control" type="text" name="username" placeholder="Username" autofocus autocomplete="off" required data-error="Silahkan masukan username anda">
          </div>
          <div class="form-group">
            <label class="control-label">Password</label>
            <input class="form-control" type="password" name="password" placeholder="Password" required data-error="Silahkan masukan password anda">
          </div>
          <div class="form-group btn-container">
            <button class="btn btn-primary btn-block" name="login">Login <i class="fa fa-sign-in fa-lg"></i></button><br>
            <p class="copyright">Copyright &copy; 2019 Puslitbang Geologi Kelautan</p>
          </div>
        </form>
      </div>
    </section>
  </body>
  <script src="assets/js/jquery-2.1.4.min.js"></script>
  <script src="assets/js/jquery-simple-validator.min.js"></script>
  <script src="assets/js/essential-plugins.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/plugins/pace.min.js"></script>
  <script src="assets/js/main.js"></script>
</html>
