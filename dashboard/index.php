<?php include_once ("_include/header.php"); ?>

        <div class="page-title">
            <h1><i class="fa fa-dashboard"></i> Statistik Aplikasi Enkripsi dan Dekripsi Puslitbang Geologi Kelautan</h1>
        </div>
        <div>
            <ul class="breadcrumb">
                <li><i class="fa fa-home fa-lg"></i></li>
                <li><a href="#">Dashboard</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="widget-small primary">
                                    <i class="icon fa fa-users fa-3x"></i>
                                    <div class="info">
                                        <h4>Users</h4>
                                        <p>
                                            <b><?php echo get_total_users(); ?></b>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="widget-small info">
                                    <i class="icon fa fa-file fa-3x"></i>
                                    <div class="info">
                                        <h4>Files</h4>
                                        <p><b><?php echo get_total_encrypt_files(); ?></b></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php include_once ("_include/footer.php"); ?>
