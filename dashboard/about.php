<?php include_once ("_include/header.php"); ?>

        <div class="page-title">
          <div>
            <h1><i class="fa fa-info"></i> Tentang Aplikasi</h1>
          </div>
          <div>
            <ul class="breadcrumb">
              <li><i class="fa fa-home fa-lg"></i></li>
              <li><a href="index.php">Dashboard</a></li>
              <li>Tentang</li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <legend>Aplikasi Enkripsi dan Dekripsi Puslitbang Geologi Kelautan</legend>
                <p>Di era ini teknologi semakin maju dan pesat, dimana kerahasiaan data salah satunya yang sangat penting bagi sebuah instansi pemerintahan.<br>
                  Oleh karena itu, maka dibuatlah Aplikasi Enkripsi dan Dekripsi untuk keamanan file. <br>
                  Aplikasi ini merupakan Aplikasi untuk menjaga kerahasiaan data-data penting di Puslitbang Geologi Kelautan</p>
              </div>
            </div>
          </div>
        </div>
<?php include_once ("_include/footer.php"); ?>