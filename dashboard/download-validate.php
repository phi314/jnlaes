<?php
session_start();
global $dbconnect, $current_user, $decrypt_folder;

include_once ('../config.php');
include_once ('AES.php');

if( ! isset( $_POST['file_id'], $_POST['key'])) {
    echo "0";
    exit;
}

$file_id = $_POST["file_id"];
$key = md5($_POST["key"]);

// If is manager by pass the key
if( $current_user['role_id'] == 2 ) {
    $file = get_file_by_id( $file_id );
    $key = $file['password'];
}

// Jika file id dan key sesuai
if( $file = get_file_by_id_and_key($file_id, $key)) {
    echo "1";
}
else {
    echo "0";
}

exit;
?>
