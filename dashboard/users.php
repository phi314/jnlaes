<?php include_once ("_include/header.php"); ?>

<?php
    $notification = '';
    if( isset( $_GET['notification'] )) {
        $notification = $_GET['notification'];
    }
    $notification_type = 'success';
    $notification_icon = 'check';

?>

        <div class="page-title">
            <h1><i class="fa fa-dashboard"></i> Users Puslitbang Geologi Kelautan</h1>
            <div>
                <ul class="breadcrumb">
                    <li><i class="fa fa-home fa-lg"></i></li>
                    <li><a href="#">Dashboard</a></li>
                    <li>List User</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <?php if( ! empty($notification)) : ?>
                    <div class="alert alert-<?php echo $notification_type; ?>">
                        <strong><i class="fa fa-<?php echo $notification_icon; ?>"></i></strong> <?php echo $notification; ?>
                    </div>
                <?php endif; ?>

                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="file" class="table striped">
                                <thead>
                                    <tr>
                                        <?php if( $current_user['role_id'] == '1') : ?>
                                        <th>Username</th>
                                        <?php endif; ?>
                                        <th>NIP</th>
                                        <th>Nama</th>
                                        <th>Jabatan</th>
                                        <?php if( $current_user['role_id'] == '1') : ?>
                                        <th>Terakhir Login</th>
                                        <th>Roles</th>
                                        <th>Edit</th>
                                        <th>Hapus</th>
                                        <?php endif; ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $query = mysqli_query($dbconnect,"SELECT users.*, roles.name as role FROM users JOIN roles ON roles.id=users.role_id");
                                    while ($data = mysqli_fetch_array($query)) { ?>
                                        <tr>
                                            <?php if( $current_user['role_id'] == '1') : ?>
                                            <td><?php echo $data['username']; ?></td>
                                            <?php endif; ?>
                                            <td><?php echo $data['nip']; ?></td>
                                            <td><?php echo $data['fullname']; ?></td>
                                            <td><?php echo $data['job_title']; ?></td>
                                            <?php if( $current_user['role_id'] == '1') : ?>
                                            <td><?php echo $data['last_activity']; ?></td>
                                            <td><?php echo $data['role']; ?></td>
                                            <td><a href="user.php?username=<?php echo $data['username']; ?>" class="btn btn-link"><i class="fa fa-edit"></i></a> </td>
                                            <td>
                                                <?php if ($current_user['username'] != $data['username']) : ?>
                                                <form action="user.php?username=<?php echo $data['username']; ?>" method="post" class="delete-user">
                                                    <input type="hidden" name="username" value="<?php echo $data['username']; ?>">
                                                    <input type="hidden" name="delete" value="delete">
                                                    <button class="btn btn-link" name="delete" value="delete"><i class="fa fa-trash"></i></button>
                                                </form>
                                                <?php endif; ?>
                                            </td>
                                            <?php endif; ?>
                                        </tr>
                                      <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php include_once ("_include/footer.php"); ?>

