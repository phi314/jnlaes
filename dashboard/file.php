<?php include_once ("_include/header.php"); ?>

<?php
    global $dbconnect, $decrypt_folder;

    if( empty( $_GET['id']) ) {
        header("location:../index.php");
    }

    $file = get_file_by_id( $_GET['id'] );
    $notification = '';
    $notification_type = 'success';
    $notification_icon = 'check';

    /*
     * Update Handler
     */
    if( isset( $_POST['update'])) {
        $title = $_POST['title'];
        $keterangan = $_POST['keterangan'];
        $id_file = $_POST['id'];

        $query = mysqli_query( $dbconnect, "UPDATE file SET title='$title', keterangan='$keterangan' WHERE id_file='$id_file'");
        $file = get_file_by_id( $id_file );
        $notification = "Berhasil update detail file";
    }

    /*
     * Delete Handler
     * Todo: Delete files
     * Delete record in database and delete file on directory
     */
    if( isset($_POST['delete'])) {
        $id_file = $_POST['id'];
        $file_decrypt = $decrypt_folder . '/' . $file['file_name_source'];
        $file_encrypt = 'file_encrypt/' . $file['file_name_finish'];
        $query = mysqli_query( $dbconnect, "DELETE FROM file WHERE id_file='$id_file'");

        // Remove the files
//        unlink($file_decrypt);
        unlink($file_encrypt);

        // Redirect
        header("location: history.php?notification=Berhasil hapus file");
        exit;
    }
?>

        <div class="page-title">
            <div>
                <h1><i class="fa fa-file"></i> <?php echo $file['title']; ?></h1>
            </div>
            <div>
                <ul class="breadcrumb">
                    <li><i class="fa fa-home fa-lg"></i></li>
                    <li><a href="index.php">Dashboard</a></li>
                    <li><a href="history.php">List File</a></li>
                    <li><?php echo $file['title']; ?></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <?php if( ! empty($notification)) : ?>
                    <div class="alert alert-<?php echo $notification_type; ?>">
                        <strong><i class="fa fa-<?php echo $notification_icon; ?>"></i></strong> <?php echo $notification; ?>
                    </div>
                <?php endif; ?>

                <div class="card">
                    <div class="card-body">
                        <form class="form-horizontal" method="post" action="">
                            <fieldset>
                                <legend>Form Update File</legend>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="title">File</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="title" type="text" value="<?php echo $file['file_name_source'];?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="title">Judul</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="title" type="text" placeholder="Judul" name="title" value="<?php echo $file['title'];?>" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="keterangan">Keterangan</label>
                                    <div class="col-lg-4">
                                        <textarea class="form-control" id="keterangan" placeholder="Keterangan" name="keterangan"><?php echo $file['keterangan']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="update"></label>
                                    <div class="col-lg-4">
                                        <input type="hidden" name="id" value="<?php echo $file['id_file']; ?>">
                                        <input type="submit" name="update" value="Update" class="form-control btn btn-primary">
                                    </div>
                                </div>

                      </fieldset>
                    </form>
              </div>
            </div>
          </div>
        </div>

<?php include_once ("_include/footer.php"); ?>
