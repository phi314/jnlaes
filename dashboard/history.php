<?php
    global $dbconnect, $current_user;
?>

<?php include_once ("_include/header.php"); ?>

<?php
$notification = '';
if( isset( $_GET['notification'] )) {
    $notification = $_GET['notification'];
}
$notification_type = 'success';
$notification_icon = 'check';

if( isset($_GET['notification_type'])) {
    $notification_type = $_GET['notification_type'];
}

if( $notification_type == 'danger') {
    $notification_icon = 'times';
}

// Jika punya file id
$id_file = '';
if( isset( $_GET['id_file'])) {
    $id_file = $_GET['id_file'];
}


?>


        <div class="page-title">
            <h1><i class="fa fa-dashboard"></i> List File Puslitbang Geologi Kelautan</h1>
            <div>
                <ul class="breadcrumb">
                    <li><i class="fa fa-home fa-lg"></i></li>
                    <li><a href="#">Dashboard</a></li>
                    <li>List File</li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <div id="notification">
                    <?php if( ! empty($notification)) : ?>
                        <div class="alert alert-<?php echo $notification_type; ?>">
                            <strong><i class="fa fa-<?php echo $notification_icon; ?>"></i></strong> <?php echo $notification; ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="file" class="table striped">
                                <thead>
                                    <tr>
                                        <th>Judul</th>
                                        <th>Keterangan</th>
<!--                                        <th>File</th>-->
                                        <th>Ukuran File</th>
                                        <th>Pengunggah</th>
                                        <th>Tanggal</th>
                                        <th>Download</th>
                                        <?php if( $current_user['role_id'] == '1') : ?>
                                            <th>Edit</th>
                                            <th>Hapus</th>
                                        <?php endif; ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $query = mysqli_query($dbconnect,"SELECT file.*, users.fullname FROM file JOIN users ON users.username=file.username ORDER BY title ASC");
                                    while ($data = mysqli_fetch_array($query)) { ?>
                                        <tr class="<?php echo $id_file == $data['id_file'] ? 'active success' : ''; ?>">
                                            <td class="file-judul"><?php echo $data['title']; ?></td>
                                            <td><?php echo empty($data['keterangan']) ? '-' : $data['keterangan']; ?></td>
<!--                                            <td>--><?php //echo $data['file_name_source']; ?><!--</td>-->
                                            <td><?php echo round($data['file_size'], 2); ?> KB</td>
                                            <td><?php echo $data['fullname']; ?></td>
                                            <td><?php echo $data['tgl_upload']; ?></td>
                                            <td>
                                                <form action="download-real.php" class="form-download input-group input-group-sm" method="post">
                                                    <?php if( $current_user['role_id'] == '2') : ?>
                                                    <input type="hidden" name="key" value="<?php echo sha1(date('YmdHis')); ?>">
                                                    <?php else : ?>
                                                    <input type="password" name="key" placeholder="password" class="form-control">
                                                    <?php endif; ?>
                                                    <input type="hidden" name="file_id" value="<?php echo $data['id_file']; ?>">
                                                    <div class="input-group-btn">
                                                        <button class="btn btn btn-success"><i class="fa fa-download"></i></button>
                                                    </div>
                                                </form>
                                            </td>
                                            <?php if( $current_user['role_id'] == '1') : ?>
                                            <td>
                                                <a href="file.php?id=<?php echo $data['id_file']; ?>" class="btn btn-link"><i class="fa fa-edit"></i> </a>
                                            </td>
                                            <td>
                                                <form action="file.php?id=<?php echo $data['id_file']; ?>" method="post" class="delete-file">
                                                    <input type="hidden" name="id" value="<?php echo $data['id_file']; ?>">
                                                    <input type="hidden" name="delete" value="delete">
                                                    <button class="btn btn-link" name="delete" value="delete"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                            <?php endif; ?>
                                        </tr>
                                      <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php include_once ("_include/footer.php"); ?>
