<?php include_once ("_include/header.php"); ?>

        <div class="page-title">
            <div>
                <h1><i class="fa fa-file"></i> Form Enkripsi Puslitbang Geologi Kelautan</h1>
            </div>
            <div>
                <ul class="breadcrumb">
                    <li><i class="fa fa-home fa-lg"></i></li>
                    <li><a href="index.php">Dashboard</a></li>
                    <li>Form Enkripsi</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form class="form-horizontal" method="post" action="encrypt-process.php" enctype="multipart/form-data">
                            <fieldset>
                                <legend>Form Enkripsi</legend>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="inputPassword">Tanggal</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="inputTgl" type="text" placeholder="Tanggal" name="datenow" value="<?php echo date("Y-m-d");?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="title">Judul</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="title" type="text" placeholder="Judul" name="title" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="inputFile">File</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="inputFile" placeholder="Input File" type="file" name="file" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="inputPassword">Password</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="inputPassword" type="password" placeholder="Password" name="pwdfile" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="textArea">Keterangan</label>
                                    <div class="col-lg-4">
                                        <textarea class="form-control" id="textArea" rows="3" name="desc" placeholder="Keterangan"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="textArea"></label>
                                    <div class="col-lg-2">
                                        <input type="submit" name="encrypt_now" value="Enkripsi File" class="form-control btn btn-primary">
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

<?php include_once ("_include/footer.php"); ?>

