<?php
session_start();

global $dbconnect;

include_once('../config.php');
include_once('../functions.php');

if(empty($_SESSION['username'])){
    header("location:../index.php");
}

$user = get_current_user_login();

?>
<!DOCTYPE html>
<html>
<head>
    <title>Aplikasi Enkripsi dan Dekripsi Puslitbang Geologi Kelautan</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="../assets/images/favicon.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/main-child.css">
</head>
<body class="sidebar-mini fixed">
<div class="wrapper">

    <header class="main-header hidden-print">
<!--        <a class="logo" href="index.php" style="font-size:13pt">Puslitbang Geologi Kelautan</a>-->
        <a class="logo" href="index.php" style="font-size:13pt">
            <img src="../assets/images/logo - 2.png" style="width: 140px; margin-bottom: 10px">
        </a>
        <nav class="navbar navbar-static-top">
            <a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
            <div class="navbar-custom-menu">
                <ul class="top-nav">
                    <li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user fa-lg"></i></a>
                        <ul class="dropdown-menu settings-menu">
                            <li><a href="logout.php"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="main-sidebar hidden-print">
        <section class="sidebar">
            <div class="user-panel">
                <div class="info">
                    <sup><?php echo $user['fullname']; ?></sup>
                    <p class="designation"><?php echo $user['job_title']; ?></p>
                    <p class="designation" style="font-size:6pt;">Aktivitas Terakhir: <?php echo $user['last_activity'] ?></p>
                </div>
            </div>
            <ul class="sidebar-menu">
                <li class=""><a href="index.php"><i class="fa fa-dashboard"></i><span> Dashboard</span></a></li>

                <?php if( $user['role_id'] == '3'): ?>
                    <li><a href="encrypt.php"><i class="fa fa-upload"></i><span> Upload file</span></a></li>
                <?php endif; ?>

                <li><a href="history.php"><i class="fa fa-list-alt"></i><span> List File</span></a></li>

                <?php if( $user['role_id'] == '1'): ?>
                <li class="treeview">
                    <a href="#"><i class="fa fa-users"></i><span>User</span><i class="fa fa-angle-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="users.php"><i class="fa fa-users"></i> List User</a></li>
                        <li><a href="user-add.php"><i class="fa fa-user-plus"></i> Tambah User</a></li>
                    </ul>
                </li>
                <?php elseif($user['role_id'] == '2') : ?>
                    <li><a href="users.php"><i class="fa fa-users"></i><span> List User</span></a></li>
                <?php endif; ?>

                <li><a href="about.php"><i class="fa fa-info"></i><span> Tentang</span></a></li>
                <li><a href="help.php"><i class="fa fa-question-circle"></i><span> Bantuan</span></a></li>
            </ul>
        </section>
    </aside>

    <div class="content-wrapper">
