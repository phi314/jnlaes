
        </div> <!-- /. Content Wrapper -->


        <footer class="col-md-10 col-md-offset-2">
            <div class="text-center">
                <img src="../assets/images/logo - 1.png" alt="" style="width: 250px">
                <br>
                <p class="copyright">Copyright &copy; 2019 Puslitbang Geologi Kelautan</p>
            </div>
        </footer>
        <script src="../assets/js/jquery-2.1.4.min.js"></script>
        <script src="../assets/js/jquery-simple-validator.min.js"></script>
        <script src="../assets/js/essential-plugins.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/plugins/datatables/js/jquery.dataTables.js"></script>
        <script src="../assets/js/plugins/pace.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
        <script src="../assets/js/main.js"></script>
    </body>
</html>
