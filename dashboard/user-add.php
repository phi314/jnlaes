<?php include_once ("_include/header.php"); ?>

<?php
global $dbconnect, $list_jabatan;

$notification = '';
$notification_type = 'success';
$notification_icon = 'check';
/*
 * Update Handler
 */
if( isset( $_POST['insert'])) {
    $username = $_POST['username'];
    $nip = $_POST['nip'];
    $fullname = $_POST['fullname'];
    $password = $_POST['password'];
    $job_title = $_POST['job_title'];
    $role_id = $_POST['role'];

    $query = "INSERT INTO users(username, password, nip, fullname, job_title, join_date, role_id) VALUES ('$username', MD5('$password'), '$nip', '$fullname', '$job_title', NOW(), '$role_id')";
    $insert = mysqli_query( $dbconnect, $query);

    if( mysqli_errno( $dbconnect ) == 1062) {
        $notification = "Username $username sudah ada";
        $notification_type = 'danger';
        $notification_icon = 'times';
    }
    else {
        $notification = "Berhasil tambah user $fullname";
    }
}

?>

        <div class="page-title">
            <div>
                <h1><i class="fa fa-user-plus"></i> Tambah User</h1>
            </div>
            <div>
                <ul class="breadcrumb">
                    <li><i class="fa fa-home fa-lg"></i></li>
                    <li><a href="index.php">Dashboard</a></li>
                    <li>User</li>
                    <li>Tambah User</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <?php if( ! empty($notification)) : ?>
                    <div class="alert alert-<?php echo $notification_type; ?>">
                        <strong><i class="fa fa-<?php echo $notification_icon; ?>"></i></strong> <?php echo $notification; ?>
                    </div>
                <?php endif; ?>

                <div class="card">
                    <div class="card-body">
                        <form class="form-horizontal" method="post" action="" validate=true>
                            <fieldset>
                                <legend>Form Tambah User</legend>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="username">Username</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="username" type="text" placeholder="Username" name="username" required autocomplete="off" data-error="Silahkan masukan username">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="nip">NIP</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="nip" type="text" placeholder="Nomor Induk Pegawai" name="nip" required autocomplete="off" data-error="Silahkan masukan nip">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="fullname">Nama Lengkap</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="fullname" type="text" placeholder="Nama Lengkap" name="fullname" required autocomplete="off" data-error="Silahkan masukan nama lengkap">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="job_title">Jabatan</label>
                                    <div class="col-lg-4">
                                        <select name="job_title" id="job_title" class="form-control">
                                            <?php
                                            foreach ($list_jabatan as $jabatan) :
                                                ?>
                                                <option><?php echo $jabatan; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="role">Role</label>
                                    <div class="col-lg-4">
                                        <select name="role" class="form-control">
                                            <?php
                                            $roles = get_roles();
                                            foreach ($roles as $role) :
                                            ?>
                                                <option value="<?php echo $role['id']; ?>"><?php echo $role['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="password">Password</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="password" type="password" placeholder="Password" name="password" required data-error="Silahkan masukan password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="insert"></label>
                                    <div class="col-lg-4">
                                        <input type="submit" name="insert" value="Tambah" class="form-control btn btn-primary">
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>

<?php include_once ("_include/footer.php"); ?>
