<?php include_once ("_include/header.php"); ?>
        <div class="page-title">
          <div>
            <h1><i class="fa fa-dashboard"></i> Statistik Aplikasi Enkripsi dan Dekripsi Puslitbang Geologi Kelautan</h1>
          </div>
          <div>
            <ul class="breadcrumb">
              <li><i class="fa fa-home fa-lg"></i></li>
              <li><a href="#">Bantuan</a></li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <legend>Bantuan Penggunaan Aplikasi</legend>
                <li>Menu Dashboard merupakan statistik dari penggunaan Aplikasi ini.</li>
                <li>Menu Form terbagi 2 yakni Form Enkripsi dan Form Dekripsi</li>
                <li>Untuk Mengenkripsi file pilih pada menu Form -> Enkripsi</li>
                <li>Untuk Mengdekripsi file pilih pada menu Form -> Dekripsi</li>
                <li>Menu Daftar List merupakan menu untuk melihat dafar list file yang telah dienkripsi dan didekripsi</li>
                <li>Menu Tentang merupakan tentang dari Aplikasi ini.</li>
                <li>Menu Bantuan merupakan menu untuk membantu penggunaan Aplikasi ini.</li>
              </div>
            </div>
          </div>
        </div>
<?php include_once ("_include/footer.php"); ?>
