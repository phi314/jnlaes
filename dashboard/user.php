<?php include_once ("_include/header.php"); ?>

<?php
    global $dbconnect, $list_jabatan;

    if( empty( $_GET['username']) ) {
        header("location:../index.php");
    }

    $user = get_user( $_GET['username'] );

    /*
     * Update Handler
     */
    if( isset( $_POST['update'])) {
        $username = $_GET['username'];
        $nip = $_POST['nip'];
        $fullname = $_POST['fullname'];
        $password = $_POST['password'];
        $job_title = $_POST['job_title'];
        $role_id = $_POST['role'];

        $query = mysqli_query( $dbconnect, "UPDATE users SET nip='$nip', fullname='$fullname', job_title='$job_title', password=MD5('$password'), role_id='$role_id' WHERE username='$username'");
        $user = get_user( $_GET['username'] );

        $notification = 'Berhasil update data user';
        $notification_type = 'success';
        $notification_icon = 'check';
    }

    /*
     * Delete Handler
     * Todo: Delete user
     * Cannot delete if user has upload a file
     */
    if( isset($_POST['delete'], $_GET['username'])) {
        $username = $_GET['username'];
        $query = mysqli_query( $dbconnect, "DELETE FROM users WHERE username='$username'");
        header("location: users.php?notification=Berhasil hapus user");
        exit;
    }
?>

        <div class="page-title">
            <div>
                <h1><i class="fa fa-user"></i> <?php echo $user['fullname']; ?></h1>
            </div>
            <div>
                <ul class="breadcrumb">
                    <li><i class="fa fa-home fa-lg"></i></li>
                    <li><a href="index.php">Dashboard</a></li>
                    <li><a href="users.php">User</a></li>
                    <li><?php echo $user['fullname']; ?></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <?php if( ! empty($notification)) : ?>
                    <div class="alert alert-<?php echo $notification_type; ?>">
                        <strong><i class="fa fa-<?php echo $notification_icon; ?>"></i></strong> <?php echo $notification; ?>
                    </div>
                <?php endif; ?>

                <div class="card">
                    <div class="card-body">
                        <form class="form-horizontal" method="post" action="" validate="true">
                            <fieldset>
                                <legend>Form Update User</legend>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="username">Username</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="username" type="text" placeholder="Username" name="username" value="<?php echo $user['username'];?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="nip">NIP</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="nip" type="text" placeholder="Nomor Induk Pegawai" name="nip"  value="<?php echo $user['nip']; ?>" required autocomplete="off" data-error="Silahkan masukan nip">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="fullname">Nama Lengkap</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="fullname" type="text" placeholder="Nama Lengkap" name="fullname" value="<?php echo $user['fullname']; ?>" required autocomplete="off" data-error="Silahkan masukan nama lengkap">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="job_title">Jabatan</label>
                                    <div class="col-lg-4">
                                        <select name="job_title" id="job_title" class="form-control">
                                            <?php
                                                foreach ($list_jabatan as $jabatan) :
                                            ?>
                                                <option <?php echo $jabatan == $user['job_title'] ? 'selected="selected"' : ''; ?>><?php echo $jabatan; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="role">Role</label>
                                    <div class="col-lg-4">
                                        <select name="role" class="form-control">
                                            <?php
                                            $roles = get_roles();
                                            foreach ($roles as $role) :
                                                ?>
                                                <option value="<?php echo $role['id']; ?>" <?php echo $role['id'] == $user['role_id'] ? "selected='selected'" : ''; ?>><?php echo $role['name']; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="password">Password</label>
                                    <div class="col-lg-4">
                                        <input class="form-control" id="password" type="password" placeholder="Password" name="password" required data-error="Silahkan masukan password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="update"></label>
                                    <div class="col-lg-4">
                                        <input type="submit" name="update" value="Update" class="form-control btn btn-primary">
                                    </div>
                                </div>

                      </fieldset>
                    </form>
              </div>
            </div>
          </div>
        </div>

<?php include_once ("_include/footer.php"); ?>
