<?php
ini_set('max_execution_time', -1);
ini_set('memory_limit', -1);
session_start();

global $dbconnect, $current_user, $decrypt_folder;

include_once ('../config.php');
include_once ('AES.php');

if( ! isset( $_GET['file_id'], $_GET['key'])) {
    // redirect with wrong password
    header('location: history.php?notification=Maaf, Password tidak sesuai&notification_type=danger');
    exit;
}

$file_id = $_GET["file_id"];
$key = md5($_GET["key"]);

// If is manager by pass the key
if( $current_user['role_id'] == 2 ) {
    $file = get_file_by_id( $file_id );
    $key = $file['password'];
}

// Jika file id dan key sesuai
if( $file = get_file_by_id_and_key($file_id, $key)) {
    $file_decrypt_path = $decrypt_folder.'/'.$file['file_name_source'];

    // Lakukan decrypt file
    $file_decrypt = decrypt_file($file_id, $key); // lakukan dekripsi file
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Cache-Control: private', false); // required for certain browsers
    header("Content-Disposition: attachment; filename=\"" . basename($file_decrypt) . "\"");
    header("Content-Type: application/force-download");
    header("Content-Length: " . filesize($file_decrypt));
    header("Connection: close");
    readfile($file_decrypt);
    unlink($file_decrypt_path); // hapus file yang sudah didekripsi
    exit;
}
else {
    // redirect with wrong password
    header('location: history.php?notification=Maaf, Password tidak sesuai&notification_type=danger');
    exit;
}
?>
