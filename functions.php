<?php

/**
 * Get current user login detail
 * @return array|null
 */
function get_current_user_login() {
    global $dbconnect;
    $data = false;
    if( isset($_SESSION['username'])) {
        $user = $_SESSION['username'];
        $query = mysqli_query($dbconnect, "SELECT users.*, roles.name as role FROM users JOIN roles ON roles.id=users.role_id WHERE username='$user'");
        $data = mysqli_fetch_array($query);
    }
    return $data;
}

function get_user( $username ) {
    global $dbconnect;
    $query = mysqli_query($dbconnect, "SELECT * FROM users WHERE username='$username' LIMIT 1");
    $data = mysqli_fetch_array($query);
    return $data;
}

function get_roles() {
    global $dbconnect;
    $roles = [];
    $query = mysqli_query($dbconnect, "SELECT * FROM roles ORDER BY id DESC");
    while ($data = mysqli_fetch_array($query) ) {
        $roles[] = $data;
    }
    return $roles;
}
get_roles();

/**
 * Update user last activity
 */
function update_last_activity() {
    global $dbconnect;
    $user = $_SESSION['username'];
    $sqlupdate = "UPDATE users SET last_activity=now() WHERE username='$user'";
    mysqli_query($dbconnect, $sqlupdate);
}

/**
 * Get total user
 * @return mixed
 */
function get_total_users() {
    global $dbconnect;
    $query = mysqli_query($dbconnect, "SELECT count(*) totaluser FROM users");
    $datauser = mysqli_fetch_array($query);
    return $datauser['totaluser'];
}

function get_total_files() {
    global $dbconnect;
    $query = mysqli_query($dbconnect,"SELECT count(*) totalencrypt FROM file");
    $dataencrypt = mysqli_fetch_array($query);
    return $dataencrypt['totalencrypt'];
}

function get_total_encrypt_files() {
    global $dbconnect;
    $query = mysqli_query($dbconnect,"SELECT count(*) totalencrypt FROM file WHERE status='1'");
    $dataencrypt = mysqli_fetch_array($query);
    return $dataencrypt['totalencrypt'];
}

function get_total_decrypt_files() {
    global $dbconnect;
    $query = mysqli_query($dbconnect,"SELECT count(*) totaldecrypt FROM file WHERE status='2'");
    $datadecrypt = mysqli_fetch_array($query);
    return $datadecrypt['totaldecrypt'];
}

function get_file_by_id($id_file) {
    global $dbconnect;
    $query = "SELECT * FROM file WHERE id_file = '$id_file' LIMIT 1";
    $result = mysqli_query( $dbconnect, $query);
    $data = (array) mysqli_fetch_object( $result );
    return $data;
}

function get_file_by_id_and_key($id_file, $key) {
    global $dbconnect;
    $query = "SELECT * FROM file WHERE id_file = '$id_file' AND password = '$key' LIMIT 1";
    $result = mysqli_query( $dbconnect, $query);
    $data = (array) mysqli_fetch_object( $result );
    return $data;
}

function decrypt_file($id_file, $key) {
    global $dbconnect, $decrypt_folder;
    $cache = false;
    $query = "SELECT * FROM file WHERE id_file = '$id_file' AND password = '$key' LIMIT 1";
    $result = mysqli_query( $dbconnect, $query);
    $data = (array) mysqli_fetch_object( $result );
    if( $data ) {
        $file_path = $data["file_url"];
        $file_name = $data["file_name_source"];
        $file_size = filesize($file_path);
        $aes = new AES($key);
        $fopen1 = fopen($file_path, "rb");
        $cache = "$decrypt_folder/$file_name";

        // Hitung total character
        $mod = $file_size % 16;
        if ($mod == 0) {
            $banyak = $file_size / 16;
        } else {
            $banyak = ($file_size - $mod) / 16;
            $banyak = $banyak + 1;
        }

        // Write decrypted file
        $fopen2 = fopen($cache, "wb");
        for ($bawah = 0; $bawah < $banyak; $bawah++) {
            $filedata = fread($fopen1, 16);
            $decrypt = $aes->decrypt($filedata);
            fwrite($fopen2, $decrypt);
        }
        fclose($fopen2);
    }

    return $cache;
}






