-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2019 at 07:17 PM
-- Server version: 10.3.11-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_aes`
--

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `id_file` int(11) NOT NULL,
  `username` varchar(15) DEFAULT NULL,
  `file_name_source` varchar(255) DEFAULT NULL,
  `file_name_finish` varchar(255) DEFAULT NULL,
  `file_url` varchar(255) DEFAULT NULL,
  `file_size` float DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `tgl_upload` timestamp NULL DEFAULT NULL,
  `status` enum('1','2') DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`id_file`, `username`, `file_name_source`, `file_name_finish`, `file_url`, `file_size`, `password`, `tgl_upload`, `status`, `title`, `keterangan`) VALUES
(12, 'user', '22724-alimi_clil_in_sociology_of_religion.pdf', '32995-alimi_clil_in_sociology_of_religion.rda', 'file_encrypt/32995-alimi_clil_in_sociology_of_religion.rda', 453.786, '81dc9bdb52d04dc20036dbd8313ed055', '2019-01-27 12:14:01', '1', 'Progressif Sosial', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'manager'),
(3, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(15) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `job_title` varchar(50) DEFAULT NULL,
  `join_date` timestamp NULL DEFAULT NULL,
  `last_activity` timestamp NULL DEFAULT NULL,
  `role_id` int(11) DEFAULT 3
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `nip`, `fullname`, `job_title`, `join_date`, `last_activity`, `role_id`) VALUES
('admin', '751cb3f4aa17c36186f4856c8982bf27', '195501091983031002', 'Ir. Kumala Harjawidjaksana, M.Sc', 'Pembina Tk. 1 - IV/b', '2017-04-28 08:48:55', '2019-01-27 12:17:05', 1),
('admin1', '751cb3f4aa17c36186f4856c8982bf27', '195012251980031002', 'Kris Budiono, M.Sc.', 'Pembina Tk. 1 - IV/b', '2019-01-22 04:08:05', NULL, 1),
('manager', 'b25265fcd7fe7294abfa9efb2f5406e1', '195311161983031005', 'Ir. Subaktian Lubis, M.Sc.', 'Pembina Utama Madya - IV/d', '2017-07-10 04:31:24', '2019-01-27 12:04:15', 2),
('manager1', 'b25265fcd7fe7294abfa9efb2f5406e1', '195310291979031002', 'Lili Samili, M.Sc.', 'Pembina Utama Madya - IV/d', '2019-01-22 04:05:28', '2019-01-27 11:21:13', 2),
('manager2', 'b25265fcd7fe7294abfa9efb2f5406e1', '195703131985031002', 'Drs. Likman Arifin', 'Pembina Utama Madya - IV/d', '2019-01-22 04:06:45', NULL, 2),
('user', '6edf26f6e0badff12fca32b16db38bf2', '195101261978031001', 'Prijantono Astjario, M.Sc.', 'Pembina - IV/a', '2019-01-12 04:08:35', '2019-01-27 12:13:44', 3),
('user1', '6edf26f6e0badff12fca32b16db38bf2', '195410011984031001', 'Drs. Syaiful Hakim', 'Pembina - IV/a', '2019-01-22 04:09:29', '2019-01-22 04:15:12', 3),
('user2', '6edf26f6e0badff12fca32b16db38bf2', '196008201986031002', 'Ir. Mustafa hanafi, M.Sc.', 'Pembina - IV/a', '2019-01-22 04:10:18', NULL, 3),
('user3', '6edf26f6e0badff12fca32b16db38bf2', '195901161985031003', 'Ir. Yudi Darlan, M.Sc.', 'Pembina - IV/a', '2019-01-22 04:11:53', NULL, 3),
('user4', '751cb3f4aa17c36186f4856c8982bf27', '195705021985031001', 'Drs. Abdul Wahid', 'Pembina - IV/a', '2019-01-22 04:12:37', NULL, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id_file`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `nip` (`nip`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `id_file` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
